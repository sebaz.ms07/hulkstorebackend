/**
 * 
 */
package com.test.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import com.test.store.HulkStoreApplication;
import com.test.store.controller.BrandController;
import com.test.store.entity.Brand;
import com.test.store.service.BrandService;

/**
 * @author Sebas
 *
 */
@ContextConfiguration(classes = HulkStoreApplication.class)
@WebMvcTest({ BrandController.class })
@TestMethodOrder(Alphanumeric.class)
class BrandTest {

	BrandController brandController;
	
	@MockBean
	BrandService brandService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@BeforeEach
	public void setup() {
		brandService = Mockito.mock(BrandService.class);
		brandController = new BrandController(brandService);
	}

	@Test
	void createTestValid() {
		 Brand brand = new Brand(null, "prueba", null);
		 when(brandService.save(brand)).thenReturn(brand);
		 ResponseEntity<Brand> httpResponse = brandController.save(brand);
		 Assertions.assertEquals(HttpStatus.OK, httpResponse.getStatusCode());
		 Assertions.assertEquals("prueba", httpResponse.getBody().getName());
		 
		logger.info("start test");
		assertTrue(true);
		logger.info("finish test");

	}
	
	@Test
	void createTestExeption() {
		 Brand brand = new Brand(null, null, null);
		 when(brandService.save(brand)).thenReturn(brand);
		 ResponseEntity<Brand> httpResponse = brandController.save(brand);
		 Assertions.assertEquals(HttpStatus.BAD_REQUEST, httpResponse.getStatusCode());

	}

}
