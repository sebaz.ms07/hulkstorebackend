/**
 * 
 */
package request;

import com.test.store.entity.Product;

/**
 * @author Sebas
 *
 */
public class ProductManageRequest {

	Product product;
	Integer quantity;

	public ProductManageRequest() {
		super();
	}

	public ProductManageRequest(Product product, Integer quantity) {
		super();
		this.product = product;
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
