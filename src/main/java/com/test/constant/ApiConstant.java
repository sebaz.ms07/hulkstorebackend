package com.test.constant;

public final class ApiConstant {

	/**
	 * ROOT
	 */
	public static final String PRODUCT_CONTROLLER_API = "api/producto";

	/**
	 * Product paths
	 */
	public static final String PRODUCT_CONTROLLER_API_GET_ALL = "buscar-productos";
	public static final String PRODUCT_CONTROLLER_API_FILTER = "filtrar-productos";
	public static final String PRODUCT_CONTROLLER_ADD = "agregar-productos";
	public static final String PRODUCT_CONTROLLER_REMOVE = "restar-productos";

	/**
	 * Brand paths
	 */

	public static final String BRAND_CONTROLLER_API = "api/marca";
	public static final String BRAND_CONTROLLER_API_SAVE = "guardar";
}
