package com.test.store.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table (name="product")
public class Product implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column (name="name")
	private String name;
	
	@Column (name ="code")
	private String code;
	
	@Column	(name= "price")
	private Integer price;
	
	@Column (name = "description")
	@Size(max = 500)
	private String description;
	
	@Column (name = "quantity")
	private Integer quantity;
	
	@OneToOne
	@JoinColumn	(name = "product_type_id")
	private ProductType productTypeId;

	@OneToOne
	@JoinColumn (name = "brand_id")
	private Brand brandId;
	
	@Column
	@CreationTimestamp
	private Date createdDate;

	public Product(Integer id, String name, String code, Integer price, @Size(max = 500) String description,
			Integer quantity, ProductType productTypeId, Brand brandId, Date createdDate) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.price = price;
		this.description = description;
		this.quantity = quantity;
		this.productTypeId = productTypeId;
		this.brandId = brandId;
		this.createdDate = createdDate;
	}

	public Product() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public ProductType getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(ProductType productTypeId) {
		this.productTypeId = productTypeId;
	}

	public Brand getBrandId() {
		return brandId;
	}

	public void setBrandId(Brand brandId) {
		this.brandId = brandId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
