package com.test.store.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.constant.ApiConstant;
import com.test.store.entity.Product;
import com.test.store.service.ProductService;

import request.ProductManageRequest;

/**
 * @author Sebas
 *
 */
@Controller
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(value = ApiConstant.PRODUCT_CONTROLLER_API)
public class ProductController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ProductService productService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping(value = ApiConstant.PRODUCT_CONTROLLER_API_GET_ALL, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Product>> getAll() {
		try {
			List<Product> listProduct= productService.getAll();
			return new ResponseEntity<>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param filter
	 * @return
	 */
	@GetMapping(value = ApiConstant.PRODUCT_CONTROLLER_API_FILTER, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Product>> filter(@RequestParam String filter) {
		try {
			List<Product> listProduct= productService.filter(filter);
			return new ResponseEntity<>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param productManageRequest
	 * @return
	 */
	@PostMapping(value = ApiConstant.PRODUCT_CONTROLLER_ADD, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> addProducts(@RequestBody ProductManageRequest productManageRequest) {
		try {
			productService.addStock(productManageRequest);
			return new ResponseEntity<>( HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 
	 * @param productManageRequest
	 * @return
	 */
	@PostMapping(value = ApiConstant.PRODUCT_CONTROLLER_REMOVE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> removeProducts(@RequestBody ProductManageRequest productManageRequest) {
		try {
			
			productService.minusStock(productManageRequest);
			return new ResponseEntity<>( HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

}
