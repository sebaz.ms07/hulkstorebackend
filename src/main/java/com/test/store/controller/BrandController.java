/**
 * 
 */
package com.test.store.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.constant.ApiConstant;
import com.test.store.entity.Brand;
import com.test.store.service.BrandService;

/**
 * @author Sebas
 *
 */
@Controller
@RequestMapping(value =ApiConstant.BRAND_CONTROLLER_API)
public class BrandController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private BrandService brandService;
	
	@Autowired
	public BrandController(BrandService brandService) {
		this.brandService = brandService;
	}
	
	@PostMapping(value = ApiConstant.BRAND_CONTROLLER_API_SAVE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Brand> save(@Valid @RequestBody Brand newBrand) {
		try {
			Brand auxBrand = brandService.save(newBrand);
			return new ResponseEntity<>(auxBrand, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
