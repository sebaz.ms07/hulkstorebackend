package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.Role;

/**
 * @author Sebas
 *
 */
public interface RoleRepository extends CrudRepository<Role, Integer> {

}
