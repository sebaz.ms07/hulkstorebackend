/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.TransactionType;

/**
 * @author Sebas
 *
 */
public interface TransactionTypeRepository extends CrudRepository<TransactionType, Integer> {

}
