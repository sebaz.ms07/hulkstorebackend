package com.test.store.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.test.store.entity.Product;


/**
 * @author Sebas
 *
 */
public interface ProductRepository extends CrudRepository<Product, Integer> {
	
	
	@Query(value = "select * from product p where upper(p.name) like upper(concat('%', :filter, '%')) or to_char(p.id, 'FM9999') = :filter", nativeQuery = true)
	public List<Product> filterProductByNameOrCode(@Param("filter") String filter);

	

}
