/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.User;

/**
 * @author Sebas
 *
 */
public interface UserRepository extends CrudRepository<User, Integer> {

}
