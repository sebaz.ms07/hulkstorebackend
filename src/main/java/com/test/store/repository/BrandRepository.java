/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.Brand;

/**
 * @author Sebas
 *
 */
public interface BrandRepository extends CrudRepository<Brand, Integer> {

}
