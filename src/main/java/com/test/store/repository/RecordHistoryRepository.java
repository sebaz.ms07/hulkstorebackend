/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.RecordHistory;

/**
 * @author Sebas
 *
 */
public interface RecordHistoryRepository extends CrudRepository<RecordHistory, Integer>{

}
