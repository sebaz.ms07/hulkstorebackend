/**
 * 
 */
package com.test.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.store.entity.ProductType;

/**
 * @author Sebas
 *
 */
public interface ProductTypeRepository extends CrudRepository<ProductType, Integer>{

}
