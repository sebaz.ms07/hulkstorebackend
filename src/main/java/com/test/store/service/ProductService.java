package com.test.store.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.test.store.entity.Product;
import com.test.store.repository.ProductRepository;

import request.ProductManageRequest;

/**
 * @author Sebas
 *
 */
@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	/**
	 * Methot that creates a new instance of a product
	 * 
	 * @param newProduct
	 * @return Product
	 */
	public Product save(@Valid Product newProduct) {
		return productRepository.save(newProduct);
	}

	/**
	 * Methot that return all the products.
	 * 
	 * @return List<Product>
	 */
	public List<Product> getAll() {
		return (List<Product>) productRepository.findAll();
	}

	/**
	 * Method that return a unique product searched it by id
	 * 
	 * @param id
	 * @return Product
	 */
	public Product findOne(Integer id) {
		return productRepository.findById(id).get();
	}

	/**
	 * Method that filter the products searching by name or code
	 * 
	 * @param filter
	 * @return List<Product>
	 */
	public List<Product> filter(String filter) {
		return productRepository.filterProductByNameOrCode(filter);
	}

	/**
	 * 
	 * @param newProductRequest
	 * @return
	 */
	public Product addStock(ProductManageRequest newProductRequest) {
		Product auxProduct = this.productRepository.findById(newProductRequest.getProduct().getId()).get();
		auxProduct.setQuantity(auxProduct.getQuantity() + newProductRequest.getQuantity());
		return this.productRepository.save(auxProduct);
	}

	/**
	 * 
	 * @param newProductRequest
	 * @return
	 */
	public Product minusStock(ProductManageRequest newProductRequest) {
		Product auxProduct = this.productRepository.findById(newProductRequest.getProduct().getId()).get();
		auxProduct.setQuantity(auxProduct.getQuantity() - newProductRequest.getQuantity());
		return this.productRepository.save(auxProduct);
	}
}
