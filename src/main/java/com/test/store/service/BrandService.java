/**
 * 
 */
package com.test.store.service;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.store.entity.Brand;
import com.test.store.repository.BrandRepository;

/**
 * @author Sebas
 *
 */

@Service
public class BrandService {


	@Autowired
	BrandRepository brandRepository;

	/**
	 * method that creates a new instance of a Brand
	 * @param newBrand
	 * @return
	 */
	public Brand save(@Valid Brand newBrand) {
		return brandRepository.save(newBrand);
	}

}
