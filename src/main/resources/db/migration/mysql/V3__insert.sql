/**Insert Utilities**/

INSERT INTO public.brand
(id, created_date, "name")
VALUES
(1,null, 'Marvel'),
(2,null, 'DC Comics'),
(3,null, 'Alternativo');


INSERT INTO public.product_type
(id, created_date, "name")
VALUES(1,null, 'Camisetas'),
(2,null, 'Vasos'),
(3,null, 'Comics'),
(4,null, 'Juguetes'),
(5,null, 'Accesorio'),
(6,null, 'Otros');


INSERT INTO public.role
(id, "name")
VALUES(1,'SELLER'),
(2,'CLIENT');

INSERT INTO public.transaction_type
(id,"name")
VALUES(1, 'Entrada'),
(2, 'Salida');


INSERT INTO public.users
(id, created_date, complete_name, role_id)
VALUES(1,null, 'Juan Martín Ochoa', 1);


INSERT INTO public.product
(code, created_date, description, "name", price, quantity, brand_id, product_type_id)
VALUES('1000', '2020-03-03', 'mug con orejas', 'mug avengers', 22000, 10, 1, 2),
('1002', '2020-03-03', 'mug con orejas', 'mug superman', 22000, 10, 2, 2),
('1001', '2020-03-03', 'mug con orejas', 'mug ironman', 23000, 8, 2, 2),
('1003', '2020-03-03', 'mug con orejas', 'mug venom', 23000, 8, 2, 2),
('1004', '2020-03-03', 'mug con orejas', 'mug aquaman', 23000, 8, 2, 2),
('1003', '2020-03-03', 'mug con orejas', 'mug mujer maravilla', 23000, 8, 2, 2),
('1003', '2020-03-03', 'camiseta', 'venom', 40000, 8, 2, 1),
('1003', '2020-03-03', 'camiseta', 'batman', 50000, 8, 2, 2),
('1003', '2020-03-03', 'headsets', 'audifonos decorados', 21000, 8, 3, 5);
